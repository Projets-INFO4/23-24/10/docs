# Dashboard

## Teamspace for GL
https://western-seer-300.notion.site/Projet-INFO4-6dc02b1e88b74207a24df31c8bd8d41e

## Week 1: 02/5 -> 02/11

- Formatting the Project Organisation : logbook, QQOQCCP, SWOT, Gantt chart

## Week 2: 02/12 -> 02/18

- Do the Specification of the project (What to do, How, ...)
- We learn how to convert an  CSV to an JSON
- On speedscope, we have a TXT to JSON executable
- Start to execute  Speedscope on local, try to read the code, understand

### Goals for the next week
- Find the good format for Speedscope
- Create some tests files 
- Finish the code review of Speedscope

## Week 3: 03/04 -> 03/10

- On speedscope, 
    * we can display a Gantt chart 
    * we have day format
- We write some CSV tests file
- We create a program that convert a CSV task file to JSON for speedscope


### Goals for the next week
- Improve conversion algorithm to obtain a more optimal graph
- Start writing user documentation
- Improve visual appearance

## Week 4: 03/11 -> 03/18

## Monday

- On format file :
    * Search interesting datas from Evalys and make sure with the teacher of the final format expected
    * Change algorithms and data's CSV format
- On speedscope :
    * Read code to know how to draw border line to separate GPU's id
    * Same to know how to draw on the same line

### Goals for Tuesday
- Finish to adapt CSV and algorithm
- Improve front-end (add border lines and draw on the same line)


## Tuesday

- Improve compiler algorithm and create tests
- Making presentation for Friday

### Goals for Friday
- Finish to do compiler
- Improve front-end (add column of GPU'id and try to increase rectangle on several lines)

## Friday

- Mid-term oral to clearly establish the project's  priority objectives.
- Writting of a Speedscope all-in-one document, to be able to work with a better understanding of the tool's architecture.

### Goals for next week
- Finish the documentation

## Week 5: 03/18 -> 03/24

## Monday

- Continuation of parcouring front-end files
- Continuation of completing our little doc

### Goals for Tuesday
- Try to withdraw parent system of the flamechart view
- Advance doc and comprehension of front-end files

## Tuesday

- Little stand-up reunion between us about summurize everything we found about speedscope so far.
- First .json file generated that corresponds to a first goal that match evalys view.

### Goals for Friday
- Change CPU line color render
- Adapt CSV compiler to the new json file.
- If possible make a first attempt to pipeline

## Friday
- Change the CPU line color render
- Finish compiler CSV to JSON
- Create a web page where json file can be downloaded
- Add button on speedscope
- Render of the time view modified

### Goals for Monday
- Make speedscope download automatically the json file from the web page
- Finish the documentation
- Make the CPU lines not hoverable and not clickable

## Week 6: 03/25 -> 03/30

## Monday
- Download automatically the json file from the web page every 10 seconds
- Adapt the background color of CPU
- Implement a button "Filter"
- Test on the Evalys CSV
- Discuss about extensions with the referent 

### Goals for Tuesday
- Finish the "Filter" view
- Red line on "Now"
- Try to save the zoom state at site refresh

## Tuesday
- "Now" line is now drawing
- Implement a vertical zoom
- Finish the CSVConverter (for filter)
- "Filter" view is printed

### Goals for Friday
- Create and use an actual time value (for "Now" line)
- Finish the "Filter" view listener
- Try to save the zoom state at site refresh
- Create a pipeline between CSVConverter and Speedscope

## Friday
- Finish the filter view listener but the function that launchs the compiler bug.
- Stand up to concert about how to make the pipeline implementation architecture.
- Implemented the pipeline with the JSON compiler (not functional yet).
- Start to write the final report.

### Goals for next week
- Correct the bug for the filter view listener that launchs the compiler.
- Finish the implementation of the pipeline.
- Test the pipeline and filter view once fully implemented.
- Progress through the writting of the final report

## Week 7: 04/01 -> 04/07

## Tuesday
- Clean server code and do a little readme
- Do last implementations of code
- Begin all documentations (presentation, readme and report)

### Goals for Wednesday
- Finish to debug some problems
- Continue to make documentation

## Wednesday and Friday
- Finish the final report, presentation slides
- Correct some code problems